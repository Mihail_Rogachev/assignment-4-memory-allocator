#ifndef TEST_H
#define TEST_H


#include <stdbool.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

bool test_1();
bool test_2();
bool test_3();
bool test_4();
bool test_5();


#endif
